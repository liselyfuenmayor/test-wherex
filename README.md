## About Test WherEx 

 Test WherEx is a web application that allows you to store customers, products and sales, it is a test for the company WherEX


- [Simple and elegan desing].
- [Easy  to run].


## HOW to run 

- **clone the git repository**
- **execute composer install **
- **execute " npm install " ]**
- **create a database with the name "test_wherex" **
- **execute " php artisan migrate --seed " **
- **execute " npm run dev" ]**
- **execute " php artisan serv" ]**
- **use any database user with the password "password" **

## Exercise two

It is located in the following url /test-wherex/test_two/query.sql

