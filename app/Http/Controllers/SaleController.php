<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Sale;
use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\SaleService; 
use App\Http\Requests\StoreSaleRequest;

/**
 * Class SaleController
 * @package App\Http\Controllers
 */
class SaleController extends Controller
{
    /**
     * @var $testService
     */
    protected $saleService;


    public function __construct(
        SaleService $saleService
    )
    {
        $this->saleService = $saleService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sales = Sale::paginate();

        return view('sale.index', compact('sales'))
            ->with('i', (request()->input('page', 1) - 1) * $sales->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $sale = new Sale();
        $clients = Client::where('status',1)->pluck('name', 'id');
        $products = Product::where('quantity', '>', 0)->get();
        return view('sale.create', compact('sale','clients','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreSaleRequest $request
     * @return Response
     */
    public function store(StoreSaleRequest $request)
    {
        try {

            $sale = Sale::create($request->all());

            $saleDetail = $this->saleService->createSaleDetail($request->details,$sale->id);
            
            if ($saleDetail['success']) 

                return redirect()->route('sales.index')
                ->with('success', 'Sale created successfully.');
            
        } catch (Exception $exception) {

            return redirect()->route('sales.index')
            ->with('error', $exception);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $sale = Sale::find($id);

        if (!$sale) {
            return redirect()->route('sales.index')
            ->with('error', 'Sale not fount.');
        }

        return view('sale.show', compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::find($id);
        $clients = Client::where('status',1)->pluck('name', 'id');
        $products = Product::where('quantity','>',0)->get();

        return view('sale.edit', compact('sale','clients','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        $sale->update($request->all());

        $saleDetail = $this->saleService->createSaleDetail($request->details,$sale->id);
        
        return redirect()->route('sales.index')
            ->with('success', 'Sale updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $sale = Sale::find($id)->delete();

        return redirect()->route('sales.index')
            ->with('success', 'Sale deleted successfully');
    }
}
