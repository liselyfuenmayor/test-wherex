<?php

namespace App\Models;

use App\Models\Client;
use App\Models\SaleDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'total_amount',
        'discount',
        'iva',
        'date_sale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to datetime types.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Returns the client object related to the  sale
     * 
     * @return Array
     */
    public function client () 
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    /**
     * Returns the sales related to the client
     * 
     * @return Array
     */
    public function details () 
    {
        return $this->HasMany(SaleDetail::class);
    }
}
