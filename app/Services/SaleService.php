<?php

namespace App\Services;

use Exception;
use App\Models\SaleDetail;
use App\Models\Sale;

class SaleService 
{
    
    
    /**
     * Response success
     * @param json $details
     * @param int $sale_id
     * @return array
     */
    public function createSaleDetail($details,$sale_id)
    {     
        try {

            $dataResponse = [
                "success" => true,
                "message" => "",
                "data" => "",
            ];

           $this->deleteSaleDetails($sale_id);

           foreach ($details as $detail) {

                $detail = json_decode($detail);

                $sale_detail = NEW SaleDetail;
                $sale_detail->sale_id = $sale_id;
                $sale_detail->product_id = $detail->id;
                $sale_detail->quantity = $detail->quantity;
                $sale_detail->subtotal = $detail->subtotal;
                $sale_detail->save(); 

                // update stock in db

                $product = $sale_detail->product ;
                $product->quantity = $product->quantity -  $sale_detail->quantity; 
                $product->save();    
            }
        
            $dataResponse['success'] = true;
            $dataResponse['message'] = "Details Create ";
            $dataResponse['data'] = $details;

            return  $dataResponse;

        } catch(Exception $exception) {

            $dataResponse['success'] = false;
            $dataResponse['message'] = $exception->getMessage();
            return $dataResponse ;


        }

    }

    public function deleteSaleDetails($sale_id)
    {
       $sale = Sale::where("id",$sale_id)->first(); 

       foreach ($sale->details as $detail) {
           $product = $detail->product;
           $product->quantity = $product->quantity + $detail->quantity; 
           $product->save(); 

           $detail->delete(); 
       }
    }

    

}