<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'price' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 1000, $max = 1000000),
            'quantity' => $this->faker->numberBetween($min = 0, $max = 25) 
        ];
    }

 
}
