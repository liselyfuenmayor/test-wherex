// developer : Lisely Fuenmayor
// date : 18/11/2021

// global vars 
var iva = $('#iva').val() ? $('#iva').val() : 0 ;
var total_amount = $('#total_amount').val() ? $('#total_amount').val() : 0 ;
var discount = $('#discount').val() ? $('#discount').val() : 0 ;


var operator_function = {
  '+' : function(x, y) {
      return x + y;
  },
  '-' : function(x, y) {
      return x - y;
  },
  '*' : function(x, y) {
      return x * y;
  }
};

// events

$('#add_product').on('show.bs.modal', function (e) {
    $("#add_product_button").css("display", "block");
    $("#edit_product_button").css("display", "none");
    $("#product").prop('disabled', false);
})

$( "#add_product_button" ).on( "click", function() {
  
  var id       = $('#product option:selected').val();
  var name     = $('#product option:selected').attr('name');
  var price    = $('#product option:selected').attr('price');
  var stock    = $('#product option:selected').attr('quantity');
  var quantity = $('#quantity').val();

    if (parseInt(quantity) > parseInt(stock)){
      $('#error').html("the stock is insufficient");
    } 
    else{
      add_new_product(id,name,price,quantity); 
      update_stock(id,stock,quantity,'-'); 
      hide_product_modal();
    }
});

$( "#edit_product_button" ).on( "click", function() {
  
  var id       = $('#product option:selected').val();
  var stock    = $('#product option:selected').attr('quantity');
  var quantity = $('#quantity').val();

    if (parseInt(quantity) > parseInt(stock + quantity)){
      $('#error').html("the stock is insufficient");
    } 
    else{
      save_edit_product(id,quantity);
      hide_product_modal();
    }
});

$("#form_sales").submit(function(e){

    e.preventDefault();

    $("#table_details tbody tr").each(function (index) {
      var id = $(this).find("td").eq(0).attr('id');
      var quantity = $(this).find("td").eq(3).html();
      var subtotal = $(this).find("td").eq(4).html();

      var detail= {
        id:   id,
        quantity: quantity,
        subtotal: subtotal
      };
      
      $("#form_sales").append("<input type='hidden' id='details"+index+"' name='details["+index+"]' value='"+JSON.stringify(detail)+"' >");
    });

    $('form').unbind('submit').submit();
});

$( ".delete_button" ).on( "click", function() {
    
  var id = $(this).attr('id'); 

  delete_product(id);
}); 

$( ".edit_button" ).on( "click", function() {
  
  var id = $(this).attr('id'); 

  edit_product(id);
}); 

// functions

function add_new_product(id,name,price,quantity) {

  var price  = parseFloat(price).toFixed(2);
  var iva_product      = ((price * 0.19) * quantity).toFixed(2);  
  var subtotal = (price * quantity).toFixed(2);  

    $('#error').html("");
      var new_row = "<tr>"+
      "<td id="+id+">"+1+"</td>"+
      "<td>"+name+"</td>"+
      "<td>"+price+"</td>"+
      "<td>"+quantity+"</td>"+
      "<td>"+subtotal+"</td>"+
      "<td>"+iva_product+"</td>"+
      "<td>"+
              "<a class='btn btn-sm btn-success edit_button' id='edit_detail_"+id+"'>Edit</a>"+
              "<a type='submit' id='delete_detail_"+id+"' class='btn btn-danger btn-sm delete_button'><i class='fa fa-fw fa-trash'></i> Delete</a>"+
      "</td>"+
    "</tr>";

    $('#details_table').append(new_row);
    addAEvents();
    update_total(iva_product ,subtotal,'+');
}

function update_total(iva_product,subtotal,operator) {

    iva          = operator_function[operator]( parseFloat(iva), parseFloat(iva_product)); 
    total_amount = operator_function[operator]( parseFloat(total_amount), parseFloat(subtotal)); 
    discount     = operator_function[operator]( parseFloat(discount), 0);

    $('#iva').val(iva.toFixed(2));
    $('#total_amount').val(total_amount.toFixed(2));
    $('#discount').val(discount);
}

function update_stock(id,stock,quantity,operator) {

    var new_stock = operator_function[operator]( parseFloat(stock), parseFloat(quantity)) ; 
    $("#product option[value="+id+"]").attr('quantity',new_stock);
}

function delete_product(id) {

  var product_id = $("#"+id).closest('tr').find("td").eq(0).attr('id');
  var quantity = $("#"+id).closest('tr').find("td").eq(3).html();
  var subtotal = $("#"+id).closest('tr').find("td").eq(4).html(); 
  var iva_product = $("#"+id).closest('tr').find("td").eq(5).html();

  var stock =   $("#product option[value="+product_id+"]").attr('quantity');

  update_total(iva_product,subtotal,'-');
  update_stock(id,stock,quantity,'+');

  $("#"+id).closest('tr').remove(); 

}

function edit_product(id) {

    var product_id = $("#"+id).closest('tr').find("td").eq(0).attr('id');
    var quantity = $("#"+id).closest('tr').find("td").eq(3).html();
  
    $("#product").val(product_id);
    $('#quantity').val(quantity);
    $('#add_product').modal("show");
    $("#product").prop('disabled', true);
    $("#add_product_button").css("display", "none");
    $("#edit_product_button").css("display", "block");
    
}

function save_edit_product(id,quantity){
  var operator_stock = "-";
  var operator_total = "+";
  var price =   $("#edit_detail_"+id).closest('tr').find("td").eq(2).html();
  var old_quantity =   $("#edit_detail_"+id).closest('tr').find("td").eq(3).html(); 
  var old_amount =   $("#edit_detail_"+id).closest('tr').find("td").eq(4).html(); 
  var old_iva_product =   $("#edit_detail_"+id).closest('tr').find("td").eq(5).html(); 
  var stock =$("#product option[value="+id+"]").attr('quantity');
  var difference = 0; 
  var amount_difference = 0; 
  var iva_difference = 0; 

  difference = Math.abs(old_quantity - quantity);
  amount_difference =  Math.abs(old_amount - (quantity * price));
  iva_difference = Math.abs(old_iva_product - (quantity * price * 0.19));

  if (parseInt(old_quantity) > parseInt(quantity)) {
      operator_stock = "+"; 
      operator_total = "-";
  }

  update_stock(id,stock,difference,operator_stock);
  
  update_total(iva_difference ,amount_difference,operator_total);

  $("#edit_detail_"+id).closest('tr').find("td").eq(3).html(quantity);
  $("#edit_detail_"+id).closest('tr').find("td").eq(4).html((quantity * price)); 
  $("#edit_detail_"+id).closest('tr').find("td").eq(5).html((quantity * price * 0.19)); 

  
}

// function to add dynamic events

function addAEvents(){
  $('.delete_button').unbind();
  $('.edit_button').unbind();
  
  $( ".delete_button" ).on( "click", function() {
    
    var id = $(this).attr('id'); 

    delete_product(id);
  }); 

  $( ".edit_button" ).on( "click", function() {
    
    var id = $(this).attr('id'); 

    edit_product(id);
  }); 
}

function hide_product_modal() {
  $('#add_product').modal("hide");
  //$("#product").val("");
  $('#quantity').val("");
}



