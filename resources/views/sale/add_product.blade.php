<div class="modal" tabindex="-1" role="dialog" id="add_product">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add product </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
             <label  for="client">Product</label>
                <select class="custom-select" id="product">
                    <option selected>Choose...</option>
                    @foreach ($products as $product)
                        <option quantity="{{ $product->quantity }}" price="{{ $product->price }}" value="{{ $product->id }}" name="{{ $product->name }}">{{ $product->name }}</option>                       
                    @endforeach
                </select>
            
        </div>
        <div class="form-group">
            <input type="number" class="form-control" id="quantity" placeholder="Quantity">
        </div>

        <div id="error" class="error"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="add_product_button"  class="btn btn-primary">Add</button>
        <button type="button" style="display:none;" id="edit_product_button"  class="btn btn-primary">Edit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
