<div class="box box-info padding-1">
    <div class="box-body">
        <div class="card-header">
            <span class="card-title">Sale Details</span> <a style="margin-left:10px;" class="btn btn-sm btn-primary " data-toggle="modal" data-target="#add_product" href=""><i class="fa fa-fw fa-eye"></i> Add New</a>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="table_details">
                    <thead class="thead">
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                            <th>Iva</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="details_table">
                                    @foreach ($sale->details as $detail)
                                        <tr>
                                            <td id="{{ $detail->product->id }}">{{ 1 }}</td>
                                            
											<td> {{ $detail->product->name }}</td>
											<td> {{ $detail->product->price }}</td>
											<td> {{ $detail->quantity  }}</td>
                                            <td> {{ $detail->subtotal }}</td>
                                            <td> {{$detail->subtotal * 0.19}}</td>
                                            <td>
                                                <a class='btn btn-sm btn-success edit_button' id='edit_detail_{{$detail->product->id}}'>Edit</a>
                                                <a type='submit' id='delete_detail_{{$detail->product->id}}' class='btn btn-danger btn-sm delete_button'><i class='fa fa-fw fa-trash'></i> Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('sale.add_product')

