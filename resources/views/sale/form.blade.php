<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            
            {{ Form::label('client_id', 'Client :')}}
            {!! Form::select('client_id', $clients, null, ['class' => 'form-control']) !!}
            
        </div>
        <div class="form-group">
            {{ Form::label('Date') }}
            {{ Form::date('date_sale', $sale->date_sale ? date('Y-m-d', strtotime($sale->date_sale)) : date('Y-m-d')  , ['class' => 'form-control' . ($errors->has('date_sale') ? ' is-invalid' : '')]) }}
            {!! $errors->first('date_sale', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('iva') }}
            {{ Form::text('iva', $sale->iva, ['class' => 'form-control' . ($errors->has('iva') ? ' is-invalid' : ''), 'placeholder' => 'Iva' ,'readonly' =>true]) }}
            {!! $errors->first('iva', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('total_amount') }}
            {{ Form::text('total_amount', $sale->total_amount, ['class' => 'form-control' . ($errors->has('total_amount') ? ' is-invalid' : ''), 'placeholder' => 'Total Amount' ,'readonly' =>true]) }}
            {!! $errors->first('total_amount', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('discount') }}
            {{ Form::text('discount', $sale->discount, ['class' => 'form-control' . ($errors->has('discount') ? ' is-invalid' : ''), 'placeholder' => 'Discount','readonly' =>true]) }}
            {!! $errors->first('discount', '<div class="invalid-feedback">:message</p>') !!}
        </div>


    </div>
    @include('sale.details')
   
</div>