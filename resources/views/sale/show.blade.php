@extends('layouts.app')

@section('template_title')
    {{ $sale->client->name ?? 'Show Sale' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">
                                <h5>Sale #  {{ $sale->id }}   </h5>
                                <div class="form-group">
                                    <strong>Client:</strong>
                                    {{ $sale->client->name }}    &nbsp;&nbsp;   <strong>Date:</strong>  {{date('d-m-Y', strtotime($sale->date_sale));}}
                                </div>

                            </span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('sales.index') }}"> Back</a>
                            <a class="btn btn-success" href="{{ route('sales.edit',$sale->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                        </div>
                    </div>

                    <div class="card-body">
                        

                        <span class="card-title">Sale Details</span>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table_details">
                                <thead class="thead">
                                    <tr>
                                        <th>#</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Iva</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    @foreach ($sale->details as $detail)
                                        <tr>
                                            <td>{{ 1 }}</td>
                                            
											<td> {{ $detail->product->name }}</td>
											<td> $ @convert($detail->product->price)</td>
											<td> {{ $detail->quantity  }}</td>
                                            <td> $ @convert($detail->subtotal)</td>
                                            <td> $ @convert($detail->subtotal * 0.19)</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                    <div class="col-md-3 resume">
                                        <div class="row number_resume">
                                            <div class="form-group">
                                                <strong>Total Amount:</strong>
                                                $ @convert($sale->total_amount)
                                            </div>
                                        </div>
                                        <div class="row number_resume">
                                                <div class="form-group">
                                                    <strong>Iva:</strong>
                                                $  @convert($sale->iva)
                                                </div>
                                        </div>
                                        <div class="row number_resume">
                                                <div class="form-group">
                                                    <strong>Discount:</strong>
                                                $  @convert($sale->discount) 
                                                </div>
                                        </div>
                                    </div>
                                
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
