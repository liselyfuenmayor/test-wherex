-- developer Lisely Fuenmayor
-- date: 19-11-2021

-- Ejercicio 2
-- SQL

-- Query 

-- 1 . Todas las personas que tengan al menos un vehículo con vehiculo.tipo 1.
    -- 2. Columnas: empresa.nombre, persona.nombre, vehiculo.descripcion, vehiculo.tipo

SELECT DISTINCT e.nombre as empresa_nombre,p.nombre as persona_nombre,
GROUP_CONCAT(v.descripcion) as vehiculo_descripcion ,v.tipo as vehiculo_tipo 
FROM persona p 
LEFT JOIN vehiculo v on v.persona_id  = p.id 
LEFT JOIN empresa e on p.empresa_id =e.id 
WHERE v.tipo = 1
GROUP BY p.id 

--3. Todas las personas de la empresa_id 3, que se tengan persona.estado 1 
    -- 4. Columnas: persona.id, persona.nombre, persona.estado

SELECT p.id, p.nombre ,p.estado 
FROM persona p 
WHERE p.empresa_id  = 3
AND p.estado = 1

-- 5. Todas las personas que tengan o no vehículo a cargo, ordenadas por empresa_id y persona.nombre.
    --6. Columnas; empresa.nombre, persona.nombre, vehiculo.descripcion

SELECT e.nombre, p.nombre ,GROUP_CONCAT(v.descripcion) as descripcion
FROM persona p 
LEFT JOIN vehiculo v on v.persona_id  = p.id 
LEFT JOIN empresa e on p.empresa_id =e.id
GROUP by p.id 
ORDER BY e.id, p.nombre