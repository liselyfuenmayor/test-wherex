-- developer Lisely Fuenmayor
-- date: 19-11-2021

-- Ejercicio 2
-- SQL

-- Db estructure 

CREATE TABLE test_wherex_2.empresa (
	id INT auto_increment NULL,
	nombre varchar(100) NOT NULL,
	estado BOOL DEFAULT 1 NULL,
	CONSTRAINT empresa_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE test_wherex_2.persona (
	id INT auto_increment NOT NULL,
	nombre varchar(100) NOT NULL,
	estado BOOL NULL,
	empresa_id INTEGER NOT NULL,
	CONSTRAINT persona_PK PRIMARY KEY (id),
	CONSTRAINT persona_FK FOREIGN KEY (empresa_id) REFERENCES test_wherex_2.empresa(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE test_wherex_2.vehiculo (
	id INT auto_increment NOT NULL,
	tipo TINYINT NOT NULL,
	descripcion varchar(100) NULL,
	persona_id INT NOT NULL,
	CONSTRAINT vehiculo_PK PRIMARY KEY (id),
	CONSTRAINT vehiculo_FK FOREIGN KEY (persona_id) REFERENCES test_wherex_2.persona(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- Insert

INSERT INTO test_wherex_2.empresa (nombre,estado) VALUES
	 ('empresa uno',1),
	 ('empresa dos',1),
	 ('empresa tres',1);

INSERT INTO test_wherex_2.persona (nombre,estado,empresa_id) VALUES
	 ('Juan Gabriel',1,3),
	 ('Marcos Antonio',0,2),
	 ('Rocio Durcal',1,1),
	 ('Ana Gabriel',0,1);

INSERT INTO test_wherex_2.vehiculo (tipo,descripcion,persona_id) VALUES
	 (1,'Optra',1),
	 (1,'Chevrolet Camion',2),
	 (1,'Corsa',2),
	 (1,'Atos',1);

